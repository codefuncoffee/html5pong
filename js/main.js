const WIDTH = 400;
const HEIGHT = 400;

const PADDLE_HEIGHT = 60;
const PADDLE_WIDTH = 20;
const BALL_RADIUS = 10;

const PADDLE_SPEED = 140; //pixels per second
const BALL_SPEED = 200; 

const ACTION_MOVE_UP = 1;
const ACTION_MOVE_DOWN = 2;
const ACTION_STAY = 3;

const RAD_TO_DEG = 180 / Math.PI;
const DEG_TO_RAD = Math.PI / 180;

let game = new Phaser.Game(WIDTH, HEIGHT, Phaser.AUTO);
let paused = true;
let lastUpdate = Date.now();

function movePaddle(paddle, action) {
    if (action == ACTION_STAY) {
        return 0;
    }

    // max distance to go
    let paddle_maxdiff = PADDLE_SPEED * (Date.now() / 1000 - lastUpdate);
    
    //choose way to go
    //if we go down
    let desiredY = paddle.y + paddle_maxdiff;
    //if we go up
    if (action === ACTION_MOVE_UP) {
        desiredY = paddle.y - paddle_maxdiff;
    }
    //check speed
    if ((desiredY - paddle.y) > paddle_maxdiff) {
        desiredY = paddle.y + paddle_maxdiff;
    } else if ((paddle.y - desiredY) > paddle_maxdiff) {
        desiredY = paddle.y - paddle_maxdiff;
    }
    
    //check end position
    if (desiredY < (PADDLE_HEIGHT / 2)) {
        desiredY = PADDLE_HEIGHT / 2;
    }
    if (desiredY > (HEIGHT - PADDLE_HEIGHT / 2)) {
        desiredY = HEIGHT - PADDLE_HEIGHT / 2;
    }
    
    paddle.y = desiredY;
}

function unPause() {
    if (paused) {
        paused = false;
        lastUpdate = Date.now() / 1000;
        game.respawnBall();
    }
}

function upPressed() {
    this.paddle1.action = ACTION_MOVE_UP;
    unPause();
}

function upUnPressed() {
    this.paddle1.action = ACTION_STAY;
}

function downPressed() {
    this.paddle1.action = ACTION_MOVE_DOWN;
    unPause();
}

function downUnPressed() {
    this.paddle1.action = ACTION_STAY;
}

function ai(paddle, ball) {
    let confidence = PADDLE_HEIGHT * 0.3;
    let desiredY = ball.y;
    let desiredAction = ACTION_STAY;
    if ((desiredY - paddle.y) >= confidence) {
        desiredAction = ACTION_MOVE_DOWN;
    } else if ((paddle.y - desiredY) > confidence) {
        desiredAction = ACTION_MOVE_UP;
    } else {
        desiredAction = ACTION_STAY;
    }
    
    paddle.action = desiredAction;
    return desiredAction;
}

function checkOverlap(spriteA, spriteB) {

    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();

    return Phaser.Rectangle.intersects(boundsA, boundsB);

}

const WALL_UP = 5;
const WALL_DOWN = 6;

function ballCollideWall(ball) {
    let upperBound = ball.y - BALL_RADIUS;
    let lowerBound = ball.y + BALL_RADIUS;
    
    if ((ball.lastWall != WALL_UP) & (upperBound < 0)) {
        ball.lastWall = WALL_UP;
        ball.energyY = -ball.energyY;
    }
    
    if ((ball.lastWall != WALL_DOWN) & (lowerBound > HEIGHT)) {
        ball.lastWall = WALL_DOWN;
        ball.energyY = -ball.energyY;
    }
}

function ballCollidePaddle(ball, p1, p2) {
    if ((ball.lastPaddle != p1) & checkOverlap(ball, p1)) {
        ball.lastWall = -1;
        ball.lastPaddle = p1;
        let paddleY = p1.y;
        let ballY = ball.y;
        
        ball.energyX = 0;
        ball.energyY = -1;
        
        //select angle depend on difference between ball.y and paddle.y
        let diff = paddleY - ballY;
        let angleOffset;
        if (diff > 0) { //paddle upper
            angleOffset = 90 * DEG_TO_RAD;
            angleOffset -= (120 / PADDLE_HEIGHT) * diff * DEG_TO_RAD;
        } else { //paddle lower
            angleOffset = 90 * DEG_TO_RAD;
            angleOffset += (120 / PADDLE_HEIGHT) * (-diff) * DEG_TO_RAD;
        }
        console.log(angleOffset);
        let eX = (ball.energyX * Math.cos(angleOffset)) - (ball.energyY * Math.sin(angleOffset));
        let eY = (ball.energyX * Math.sin(angleOffset)) + (ball.energyY * Math.cos(angleOffset));
        
        ball.energyX = eX;
        ball.energyY = eY;
    }
    if ((ball.lastPaddle != p2) & checkOverlap(ball, p2)) {
        ball.lastWall = -1;
        ball.lastPaddle = p2;
        let paddleY = p2.y;
        let ballY = ball.y;
        
        ball.energyX = 0;
        ball.energyY = -1;
        
        //select angle depend on difference between ball.y and paddle.y
        let diff = paddleY - ballY;
        let angleOffset;
        if (diff > 0) { //paddle upper
            angleOffset = -90 * DEG_TO_RAD;
            angleOffset += (90 / PADDLE_HEIGHT) * diff * DEG_TO_RAD;
        } else { //paddle lower
            angleOffset = -90 * DEG_TO_RAD;
            angleOffset -= (90 / PADDLE_HEIGHT) * (-diff) * DEG_TO_RAD;
        }
//        console.log(angleOffset);
        let eX = (ball.energyX * Math.cos(angleOffset)) - (ball.energyY * Math.sin(angleOffset));
        let eY = (ball.energyX * Math.sin(angleOffset)) + (ball.energyY * Math.cos(angleOffset));
        
        ball.energyX = eX;
        ball.energyY = eY;
    }
}

function updateScore(state) {
    state.scoreText1.text = state.paddle1.score;
    state.scoreText2.text = state.paddle2.score;
    paused = true;
}

function ballCollideWin(ball, state) {
    if (ball.x < 0) {
        state.paddle2.score++;
        updateScore(state);
    } else if (ball.x > WIDTH) {
        state.paddle1.score++;
        updateScore(state);
    }
}

function ballCollideChecks(ball, p1, p2, state) {
    ballCollideWall(ball);
    ballCollideWin(ball, state);
    ballCollidePaddle(ball, p1, p2);
}

let MainState = {
    preload: function(game) {
        game.load.image("paddleTexture", "img/paddle.png");
        game.load.image("ballTexture", "img/ball.png");
    },
    
    moveBall: function() {
        ballCollideChecks(this.ball, this.paddle1, this.paddle2, this);
        this.ball.x += BALL_SPEED * this.ball.energyX * (Date.now() / 1000 - lastUpdate);
        this.ball.y += BALL_SPEED * this.ball.energyY * (Date.now() / 1000 - lastUpdate);
    },
    
    create: function(game) {
        //set scale
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        
        //add sprites
        this.ball = game.add.sprite(0,0, "ballTexture");
        this.ball.anchor.setTo(0.5, 0.5);
        this.ball.x = WIDTH / 2;
        this.ball.y = HEIGHT / 2;
        this.ball.energyX = -1;
        this.ball.energyY = 0;
        this.ball.lastPaddle = this.paddle2;
        this.ball.lastWall = -1;
        
        this.paddle1 = game.add.sprite(0,0, "paddleTexture");
        this.paddle1.anchor.setTo(0, 0.5);
        this.paddle1.x = 0;
        this.paddle1.y = HEIGHT / 2;
        this.paddle1.action = ACTION_STAY;
        this.paddle1.score = 0;
        
        this.paddle2 = game.add.sprite(0,0, "paddleTexture");
        this.paddle2.anchor.setTo(1, 0.5);
        this.paddle2.x = WIDTH;
        this.paddle2.y = HEIGHT / 2;
        this.paddle2.action = ACTION_STAY;
        this.paddle2.score = 0;
        
        let keyW = game.input.keyboard.addKey(Phaser.Keyboard.W);
        keyW.onDown.add(upPressed, this);
        keyW.onUp.add(upUnPressed, this);
        
        let keyS = game.input.keyboard.addKey(Phaser.Keyboard.S);
        keyS.onDown.add(downPressed, this);
        keyS.onUp.add(downUnPressed, this);
        
        let style = { font: "30px Arial", fill: "#ff0044", align: "center" };

        this.scoreText1 = game.add.text(
                game.world.centerX / 2, 
                game.world.centerY / 4, "" + this.paddle1.score, 
                style);
        this.scoreText2 = game.add.text(
                game.world.centerX / 2 + game.world.centerX, 
                game.world.centerY / 4, "" + this.paddle2.score, 
                style);
                
        style = { font: "20px Arial", fill: "#ffffff", align: "center" };
        this.pausedText = game.add.text(
                game.world.centerX,
                game.world.centerY,
                "Paused. Press Any key to unpause\nUse W and S buttons to move paddle",
                style);
        this.pausedText.anchor.setTo(0.5);
        
        lastUpdate = Date.now() / 1000;
        
        let ball = this.ball;
        let paddle1 = this.paddle1;
        game.respawnBall = function() {
            ball.x = this.world.centerX;
            ball.y = this.world.centerY;
            ball.energyY = 0;
            ball.energyX = -1;
            ball.lastWall = -1;
            ball.lastPaddle = -1;
            paddle1.y = WIDTH / 2;
        };
    },
    
    update: function(game) {
        if (!paused) {
            if (this.pausedText.x > 0) {
                this.pausedText.x = -99999;
            }
            movePaddle(this.paddle2, ai(this.paddle2, this.ball));
            movePaddle(this.paddle1, this.paddle1.action);
            this.moveBall();
            lastUpdate = Date.now() / 1000;
        } else {
            if (this.pausedText.x < 0) {
                this.pausedText.x = game.world.centerX;
            }
        }
    }
};

game.state.add("MainState", MainState);
game.state.start("MainState");
